/**
 * @format
 * @flow
 */

import React, { Component } from 'react'
import { Provider } from 'react-redux'
import configureStore from 'spendee/src/redux/configureStore'
import Navigation from 'spendee/src/navigation'

type Props = {}

export default class App extends Component<Props> {
  render() {
    return (
      <Provider store={configureStore()}>
        <Navigation />
      </Provider>
    )
  }
}
