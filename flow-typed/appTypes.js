/**
 * @format
 * @flow
 */

declare type User = {
  balance: number,
}

declare type Category = {
  id: string,
  name: string,
}

declare type Transaction = {
  id: string,
  date: number,
  value: number,
  categoryId: string,
  categoryName?: string,
  note?: string,
}

declare type Action = Object

declare type DataProvider = {
  save: Function,
  remove: Function,
  loadAll: Function,
}
