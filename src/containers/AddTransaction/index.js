/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { TRANSACTIONS_ADD_ITEM } from 'spendee/src/redux/actions'
import AddTransactionView from 'spendee/src/components/AddTransactionView'
import categoriesSelector from 'spendee/src/selectors/categories'

type Props = {
  addTransaction: Function,
  categories: Array<Category>,
}

class AddTransactionContainer extends PureComponent<Props> {
  render = () => {
    const { addTransaction, categories } = this.props
    return <AddTransactionView onAddTransaction={addTransaction} categories={categories} />
  }
}

const mapStateToProps = state => ({ categories: categoriesSelector(state) })

const mapDispatchToProps = dispatch => ({ addTransaction: (transaction) => {
  dispatch({ type: TRANSACTIONS_ADD_ITEM, transaction })
} })

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddTransactionContainer)
