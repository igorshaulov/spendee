/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import UserView from 'spendee/src/components/UserView'

type Props = {
  user: Object,
  navigation: Object,
}

export class UserContainer extends PureComponent<Props> {
  render = () => {
    const { user: { balance } } = this.props
    return <UserView balance={balance} />
  }
}

const mapStateToProps = state => ({ user: state.user })

const mapDispatchToProps = () => ({})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserContainer)
