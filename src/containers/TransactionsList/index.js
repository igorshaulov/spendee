/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import TransactionsListView from 'spendee/src/components/TransactionsListView'
import transactionsSelector from 'spendee/src/selectors/transactions'
import { createTransaction } from 'spendee/src/utils/transactionBuilder'
import { TRANSACTIONS_ADD_ITEM, TRANSACTIONS_REMOVE_ITEM } from 'spendee/src/redux/actions'

type Props = {
  addTransaction: Function,
  removeTransaction: Function,
  transactions: Array<Transaction>,
}

export class TransactionsListContainer extends PureComponent<Props> {
  onAddTransactionPress = () => {
    const data: Object = { date: new Date(),
      value: -10,
      categoryId: 'some_id',
      note: 'had a drink' }

    const transaction: ?Transaction = createTransaction(data)
    if (transaction) {
      const { addTransaction } = this.props
      addTransaction(transaction)
    }
  }

  onDelete = (transaction: Transaction) => {
    const { removeTransaction } = this.props
    removeTransaction(transaction)
  }

  render = () => {
    const { transactions } = this.props
    return (
      <TransactionsListView
        onTransactionDelete={this.onDelete}
        transactions={transactions}
        onAddTransactionPress={this.onAddTransactionPress}
      />
    )
  }
}

const mapStateToProps = state => ({ transactions: transactionsSelector(state) })

const mapDispatchToProps = dispatch => ({ addTransaction: (transaction) => {
  dispatch({ type: TRANSACTIONS_ADD_ITEM, transaction })
},
removeTransaction: (transaction) => {
  dispatch({ type: TRANSACTIONS_REMOVE_ITEM, transaction })
} })

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TransactionsListContainer)
