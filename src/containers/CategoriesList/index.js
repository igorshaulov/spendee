/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import categoriesSelector from 'spendee/src/selectors/categories'
import CategoriesListView from 'spendee/src/components/CategoriesListView'
import { CATEGORIES_ADD_ITEM, CATEGORIES_REMOVE_ITEM } from 'spendee/src/redux/actions'

type Props = {
  categories: Array<Category>,
  removeCategory: Function,
  addCategory: Function,
}

export class CategoriesListContainer extends PureComponent<Props> {
  onCategoryDelete = (category: Category) => {
    const { removeCategory } = this.props
    removeCategory(category)
  }

  onCategoryAdd = (name: string) => {
    const { addCategory } = this.props
    addCategory(name)
  }

  render = () => {
    const { categories } = this.props
    return (
      <CategoriesListView
        categories={categories}
        onCategoryDelete={this.onCategoryDelete}
        onCategoryAdd={this.onCategoryAdd}
      />
    )
  }
}

const mapStateToProps = state => ({ categories: categoriesSelector(state) })

const mapDispatchToProps = dispatch => ({ addCategory: (name) => {
  dispatch({ type: CATEGORIES_ADD_ITEM, name })
},
removeCategory: (category) => {
  dispatch({ type: CATEGORIES_REMOVE_ITEM, category })
} })

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CategoriesListContainer)
