/**
 * @format
 * @flow
 */

import { createSelector } from 'reselect'

const getTransactions = state => state.transactions.items
const getCategories = state => state.categories.items

export default createSelector(
  [getTransactions, getCategories],
  (transactions: Array<Transaction>, categories: Array<Category>) => {
    let result: Array<Transaction> = transactions.sort(
      (t1: Transaction, t2: Transaction) => t2.date - t1.date,
    )

    const objCategories = {}
    categories.forEach((cat) => {
      objCategories[cat.id] = cat.name
    })

    result = result.map(transaction => ({ ...transaction,
      categoryName: objCategories[transaction.categoryId] }))
    return result
  },
)
