/**
 * @format
 * @flow
 */

import { createSelector } from 'reselect'

const getCategories = state => state.categories.items

export default createSelector([getCategories], (categories: Array<Category>) => {
  const result: Array<Category> = categories.sort((c1: Category, c2: Category) => {
    const name1 = c1.name
    const name2 = c2.name
    if (name1 < name2) return -1
    if (name1 > name2) return 1
    return 0
  })
  return result
})
