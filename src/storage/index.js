/**
 * @format
 * @flow
 */

import { AsyncStorage } from 'react-native'

export const saveObject = async (key: string, data: Object) => {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(data))
    return true
  } catch (error) {
    return false
  }
}

export const removeObject = async (key: string) => {
  try {
    await AsyncStorage.removeItem(key)
    return true
  } catch (error) {
    return false
  }
}

export const loadObject = async (key: string) => {
  try {
    const str: string = await AsyncStorage.getItem(key)
    return JSON.parse(str)
  } catch (error) {
    return false
  }
}

export const getValuesWhereKeysContaining = async (containing: string) => {
  const allKeys: Array<string> = await AsyncStorage.getAllKeys()
  const keys: Array<string> = allKeys.filter(key => key.indexOf(containing) !== -1)
  const result: Array<Array<any>> = await AsyncStorage.multiGet(keys)
  const values: Array<Transaction> = result.map(ar => JSON.parse(ar[1]))
  return values
}
