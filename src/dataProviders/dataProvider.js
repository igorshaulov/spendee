import { saveObject,
  loadObject,
  removeObject,
  getValuesWhereKeysContaining } from 'spendee/src/storage'

export default (key: string): DataProvider => {
  const keyForId = id => `${key}:${id}`

  const save = async (data: any, id?: string) => {
    const result = await saveObject(id ? keyForId(id) : key, data)
    return result
  }

  const load = async (id?: string) => {
    const result = await loadObject(id ? keyForId(id) : key)
    return result
  }

  const remove = async (id?: string) => {
    const result = await removeObject(id ? keyForId(id) : key)
    return result
  }

  const loadAll = async () => {
    const result: Array<Transaction> = await getValuesWhereKeysContaining(key)
    return result
  }

  return { save, load, remove, loadAll }
}
