import { createStackNavigator, createDrawerNavigator } from 'react-navigation'
import Menu from 'spendee/src/screens/Menu'
import Home from 'spendee/src/screens/Home'
import Categories from 'spendee/src/screens/Categories'
import AddTransaction from 'spendee/src/screens/AddTransaction'

const homeNavigator = createStackNavigator({ Home: { screen: Home } })
const categoriesNavigator = createStackNavigator({ Categories: { screen: Categories } })
const addTransactionStack = createStackNavigator({ AddTransaction: { screen: AddTransaction } })

const mainStack = createDrawerNavigator(
  { Home: { screen: homeNavigator },
    Categories: { screen: categoriesNavigator } },
  { contentComponent: Menu,
    drawerWidth: 300 },
)

export default createStackNavigator(
  { Main: { screen: mainStack },
    AddTransaction: { screen: addTransactionStack } },
  { mode: 'modal',
    headerMode: 'none' },
)
