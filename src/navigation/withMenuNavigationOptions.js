/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'

type Props = {}

const withMenuNavigationOptions = (WrappedComponent: any, headerTitle: string) => {
  class HOC extends PureComponent<Props> {
    static navigationOptions = ({ navigation }) => ({ headerTitle,
      headerLeft: (
        <TouchableOpacity style={styles.container} onPress={() => navigation.toggleDrawer()}>
          <Text>Menu</Text>
        </TouchableOpacity>
      ) })

    render = () => <WrappedComponent />
  }

  return HOC
}

const styles = StyleSheet.create({ container: { margin: 10 } })
export default withMenuNavigationOptions
