/**
 * @format
 * @flow
 */

import uuid from 'uuid/v1'

export const createTransaction = (data: Object): ?Transaction => {
  const { date, value, categoryId } = data
  if (!date || !value || !categoryId) {
    return null
  }
  return { id: uuid(), ...data }
}
