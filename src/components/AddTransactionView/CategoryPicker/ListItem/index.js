/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { CELL_BG_EVEN, CELL_BG_ODD } from 'spendee/src/Styles'

type Props = {
  category: Category,
  index: number,
}

export default class ListViewItem extends PureComponent<Props> {
  render = () => {
    const { category, index } = this.props
    const { name } = category

    const style = { ...styles.container,
      backgroundColor: index % 2 === 0 ? CELL_BG_ODD : CELL_BG_EVEN }

    return (
      <View style={style}>
        <Text style={styles.text}>{name}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({ container: { flex: 1, justifyContent: 'center', height: 40 },
  text: { paddingLeft: 10 } })
