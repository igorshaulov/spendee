/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { StyleSheet, View, FlatList, TouchableOpacity, Text } from 'react-native'
import ListItem from './ListItem'

type Props = {
  categories: Array<Category>,
  selectedCategory: Category,
  onCategorySelected: Function,
}

type State = {
  isOpened: boolean,
}

export default class CaregoryPicker extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = { isOpened: false }
  }

  onSelect = (category: Category) => {
    const { onCategorySelected } = this.props
    onCategorySelected(category)
    this.setState({ isOpened: false })
  }

  renderItem = (data: any, index: number) => {
    const category: Category = data.item
    return (
      <TouchableOpacity
        onPress={() => {
          this.onSelect(category)
        }}
      >
        <ListItem key={category.id} category={category} index={index} />
      </TouchableOpacity>
    )
  }

  render = () => {
    const { categories } = this.props
    const { isOpened } = this.state
    const { selectedCategory } = this.props
    let content
    if (isOpened) {
      content = (
        <FlatList
          data={categories}
          renderItem={this.renderItem}
          keyExtractor={(category: Category) => category.id}
        />
      )
    } else {
      content = (
        <TouchableOpacity
          onPress={() => {
            this.setState({ isOpened: true })
          }}
        >
          <Text>{selectedCategory.name}</Text>
        </TouchableOpacity>
      )
    }

    return (
      <View style={styles.container}>
        <Text>Category:</Text>
        {content}
      </View>
    )
  }
}

const styles = StyleSheet.create({ container: { flexDirection: 'column' } })
