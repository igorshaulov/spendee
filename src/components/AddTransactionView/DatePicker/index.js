/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { StyleSheet } from 'react-native'
import DatePicker from 'react-native-datepicker'

type Props = {
  date: Date,
  maxDate: Date,
  onDatePicked: Function,
}

class DatePickerComponent extends PureComponent<Props> {
  render = () => {
    const { maxDate, date, onDatePicked } = this.props
    return (
      <DatePicker
        style={styles.container}
        maxDate={maxDate}
        date={date}
        mode="date"
        placeholder="Select date"
        format="YYYY-MM-DD"
        confirmBtnText="Done"
        cancelBtnText="Cancel"
        onDateChange={onDatePicked}
      />
    )
  }
}

const styles = StyleSheet.create({ container: { backgroundColor: '#fff',
  alignSelf: 'center',
  height: 40,
  marginVertical: 10,
  marginHorizontal: 20,
  borderRadius: 10 } })

export default DatePickerComponent
