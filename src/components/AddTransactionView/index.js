/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { StyleSheet, ScrollView, KeyboardAvoidingView } from 'react-native'
import { createTransaction } from 'spendee/src/utils/transactionBuilder'
import Button from 'spendee/src/components/UI/Button'
import { withNavigation } from 'react-navigation'
import AmountInput from './AmountInput'
import DatePicker from './DatePicker'
import CatgoryPicker from './CategoryPicker'
import NoteInput from './NoteInput'

type Props = {
  categories: Array<Category>,
  onAddTransaction: Function,
  navigation: Object,
}

type State = {
  value: number,
  date: Date,
  maxDate: Date,
  note: string,
  category: Category,
  isValid: boolean,
}

class AddTransactionView extends PureComponent<Props, State> {
  constructor(props) {
    super(props)

    const category = props.categories[0]
    this.state = { value: 0,
      maxDate: new Date(),
      date: new Date(),
      note: '',
      category,
      isValid: false }
  }

  createTransactionWithState = (state: State) => {
    const { value, date, note, category } = state
    const { navigation } = this.props
    const isIncome = navigation.getParam('isIncome', false)
    const data: Object = { date: date.getTime(),
      value: isIncome ? value : -value,
      categoryId: category.id,
      note }
    const transaction: ?Transaction = createTransaction(data)
    return transaction
  }

  onAddTransaction = () => {
    const transaction: ?Transaction = this.createTransactionWithState(this.state)
    if (transaction) {
      const { onAddTransaction, navigation } = this.props
      onAddTransaction(transaction)
      navigation.dismiss()
    }
  }

  validateWithUpdatedField = (fields: Object) => {
    this.setState((prevState: State) => {
      const newState = { ...prevState, ...fields }
      const transaction = this.createTransactionWithState(newState)
      newState.isValid = !!transaction
      return newState
    })
  }

  onAmountChanged = (value: number) => {
    this.validateWithUpdatedField({ value })
  }

  onDatePicked = (str: string, date: Date) => {
    this.validateWithUpdatedField({ date })
  }

  onCategorySelected = (category: Category) => {
    this.validateWithUpdatedField({ category })
    this.setState({ category })
  }

  onNoteChanged = (note: string) => {
    this.validateWithUpdatedField({ note })
  }

  render = () => {
    const { categories } = this.props
    const { maxDate, date, note, category, isValid } = this.state
    return (
      <KeyboardAvoidingView style={styles.container}>
        <ScrollView>
          <AmountInput onChanged={this.onAmountChanged} />
          <DatePicker maxDate={maxDate} date={date} onDatePicked={this.onDatePicked} />
          <CatgoryPicker
            categories={categories}
            selectedCategory={category}
            onCategorySelected={this.onCategorySelected}
          />
          <NoteInput text={note} onChanged={this.onNoteChanged} />
          {isValid && <Button title="Add transaction" onPress={this.onAddTransaction} />}
        </ScrollView>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({ container: { flex: 1,
  backgroundColor: '#fff',
  margin: 5,
  padding: 20 } })

export default withNavigation(AddTransactionView)
