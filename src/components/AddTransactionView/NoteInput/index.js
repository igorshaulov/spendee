/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { StyleSheet, TextInput } from 'react-native'
import { GRAY_COLOR } from 'spendee/src/Styles'

type Props = {
  onChanged: Function,
  text: string,
}

class NoteInput extends PureComponent<Props> {
  onChangeText = (text: string) => {
    const { onChanged } = this.props
    onChanged(text)
  }

  render = () => {
    const { text } = this.props
    return (
      <TextInput
        style={styles.container}
        value={text}
        placeholder="Note (optional)"
        clearButtonMode="while-editing"
        onChangeText={this.onChangeText}
        multiline
        numberOfLines={0}
        maxLength={1024}
      />
    )
  }
}

const styles = StyleSheet.create({ container: { height: 120,
  marginVertical: 10,
  borderRadius: 10,
  borderWidth: 1,
  padding: 10,
  borderColor: GRAY_COLOR } })

export default NoteInput
