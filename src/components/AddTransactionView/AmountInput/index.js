/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { StyleSheet, TextInput } from 'react-native'
import { GRAY_COLOR } from 'spendee/src/Styles'

type Props = {
  onChanged: Function,
}

type State = {
  text: string,
}

class AmountInput extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = { text: '' }
  }

  onChangeText = (text: string) => {
    const { onChanged } = this.props
    const value = parseFloat(text)
    this.setState({ text }, () => {
      onChanged(value)
    })
  }

  render = () => {
    const { text } = this.state
    return (
      <TextInput
        style={styles.container}
        value={text}
        placeholder="Amount"
        keyboardType="decimal-pad"
        clearButtonMode="while-editing"
        onChangeText={this.onChangeText}
      />
    )
  }
}

const styles = StyleSheet.create({ container: { backgroundColor: '#fff',
  alignSelf: 'stretch',
  textAlign: 'center',
  height: 40,
  marginVertical: 10,
  borderRadius: 10,
  borderWidth: 1,
  borderColor: GRAY_COLOR } })

export default AmountInput
