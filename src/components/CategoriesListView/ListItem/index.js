/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import Swipeout from 'react-native-swipeout'
import { CELL_BG_EVEN, CELL_BG_ODD } from 'spendee/src/Styles'

type Props = {
  category: Category,
  onDelete: Function,
  index: number,
}

export default class ListViewItem extends PureComponent<Props> {
  onDelete = (category: Category) => {
    const { onDelete } = this.props
    onDelete(category)
  }

  render = () => {
    const { category, index } = this.props
    const { name } = category

    const style = { ...styles.container,
      backgroundColor: index % 2 === 0 ? CELL_BG_ODD : CELL_BG_EVEN }
    const swipeSettings = { autoClose: true,
      right: [
        { onPress: () => {
          this.onDelete(category)
        },
        text: 'Delete',
        type: 'delete' },
      ] }

    return (
      <Swipeout {...swipeSettings}>
        <View style={style}>
          <Text style={styles.text}>{name}</Text>
        </View>
      </Swipeout>
    )
  }
}

const styles = StyleSheet.create({ container: { flex: 1, justifyContent: 'center', height: 40 },
  text: { paddingLeft: 10 } })
