/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { StyleSheet, KeyboardAvoidingView, FlatList } from 'react-native'
import ListItem from './ListItem'
import InputHeader from './InputHeader'

type Props = {
  categories: Array<Category>,
  onCategoryDelete: Function,
  onCategoryAdd: Function,
}

export default class CaregoriesListView extends PureComponent<Props> {
  renderItem = (data: any) => {
    const category: Category = data.item
    const { onCategoryDelete } = this.props
    return (
      <ListItem
        key={category.id}
        category={category}
        index={data.index}
        onDelete={onCategoryDelete}
      />
    )
  }

  onAddCategory = (name: string) => {
    const { onCategoryAdd } = this.props
    onCategoryAdd(name)
  }

  render = () => {
    const { categories } = this.props
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding">
        <InputHeader onAddPressed={this.onAddCategory} />
        <FlatList
          style={styles.list}
          data={categories}
          renderItem={this.renderItem}
          keyExtractor={(category: Category) => category.id}
        />
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({ container: { flex: 1 }, list: { flex: 1 } })
