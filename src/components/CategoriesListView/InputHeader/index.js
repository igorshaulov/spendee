/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { StyleSheet, TextInput, View } from 'react-native'
import { GRAY_COLOR } from 'spendee/src/Styles'
import Button from 'spendee/src/components/UI/Button'

type Props = {
  onAddPressed: Function,
}

type State = {
  text: string,
  showsAddButton: boolean,
  height: number,
}

const defaultState = { showsAddButton: false, text: '', height: 40 }

export default class InputHeader extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = defaultState
  }

  onChangeText = (text: string) => {
    const hasText = text.length !== 0
    const height = hasText ? 80 : 40
    this.setState({ showsAddButton: hasText, text, height })
  }

  onButtonPress = () => {
    const { text } = this.state
    const { onAddPressed } = this.props
    onAddPressed(text)
    this.setState(defaultState)
  }

  render = () => {
    const { showsAddButton, text, height } = this.state
    const style = { ...styles.container, height }
    return (
      <View style={style}>
        <TextInput
          value={text}
          placeholder="Add a category"
          clearButtonMode="while-editing"
          onChangeText={this.onChangeText}
        />
        {showsAddButton && <Button title="Add" onPress={this.onButtonPress} />}
      </View>
    )
  }
}

const styles = StyleSheet.create({ container: { marginHorizontal: 10,
  padding: 10,
  marginVertical: 10,
  borderRadius: 10,
  borderWidth: 1,
  borderColor: GRAY_COLOR },
input: { height: 40 } })
