/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { StyleSheet, TouchableOpacity, Text } from 'react-native'
import { PRIMARY_COLOR } from 'spendee/src/Styles'

type Props = {
  title: string,
  onPress?: Function,
  backgroundColor?: string,
}

export default class Button extends PureComponent<Props> {
  static defaultProps = { onPress: () => {} }

  render = () => {
    const { title, onPress, backgroundColor = PRIMARY_COLOR } = this.props
    const style = { ...styles.container, backgroundColor }
    return (
      <TouchableOpacity onPress={onPress} style={style}>
        <Text style={styles.title}>{title}</Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({ container: { flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  height: 44,
  borderRadius: 8 },
title: { fontSize: 19, color: '#fff', marginHorizontal: 10 } })
