/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { StyleSheet, View } from 'react-native'
import { SafeAreaView } from 'react-navigation'

type Props = {
  children: any,
}

export default class ScreenWrapper extends PureComponent<Props> {
  render = () => {
    const { children } = this.props
    return (
      <SafeAreaView style={styles.safeAreaContainer}>
        <View style={styles.screenContainer}>{children}</View>
      </SafeAreaView>
    )
  }
}

export const styles = StyleSheet.create({ safeAreaContainer: { flex: 1, backgroundColor: '#eee' },
  screenContainer: { flex: 1,
    backgroundColor: '#fff',
    margin: 5 } })
