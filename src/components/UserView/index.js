/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { withNavigation } from 'react-navigation'
import Button from 'spendee/src/components/UI/Button'
import { GREEN_1, RED_1 } from 'spendee/src/Styles'

type Props = {
  balance: number,
  navigation: Object,
}

class UserView extends PureComponent<Props> {
  onAddTransaction = (isIncome: boolean) => {
    const { navigation } = this.props
    navigation.navigate('AddTransaction', { isIncome })
  }

  render = () => {
    const { balance } = this.props
    return (
      <View style={styles.container}>
        <Text>User</Text>
        <Text>{`Balance: ${balance}`}</Text>
        <View style={styles.buttonsContainer}>
          <Button
            title="Add expenses"
            onPress={() => {
              this.onAddTransaction(false)
            }}
            backgroundColor={RED_1}
          />
          <Button
            title="Add income"
            onPress={() => {
              this.onAddTransaction(true)
            }}
            backgroundColor={GREEN_1}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({ container: {},
  buttonsContainer: { flexDirection: 'row', justifyContent: 'space-between' } })

export default withNavigation(UserView)
