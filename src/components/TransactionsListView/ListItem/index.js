/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import moment from 'moment'
import Swipeout from 'react-native-swipeout'
import { CELL_BG_EVEN, CELL_BG_ODD } from 'spendee/src/Styles'

type Props = {
  transaction: Transaction,
  onDelete: Function,
  index: number,
}

export default class TrasactionsListViewItem extends PureComponent<Props> {
  onDelete = (transaction: Transaction) => {
    const { onDelete } = this.props
    onDelete(transaction)
  }

  render = () => {
    const { transaction, index } = this.props
    const { date, categoryName = 'No category', value, note } = transaction
    const when: string = moment.unix(date / 1000).fromNow()
    const style = { ...styles.container,
      backgroundColor: index % 2 === 0 ? CELL_BG_ODD : CELL_BG_EVEN }
    const swipeSettings = { autoClose: true,
      right: [
        { onPress: () => {
          this.onDelete(transaction)
        },
        text: 'Delete',
        type: 'delete' },
      ] }

    return (
      <Swipeout {...swipeSettings}>
        <View style={style}>
          <Text>{when}</Text>
          <Text>{value}</Text>
          <Text>{`Category: ${categoryName}`}</Text>
          <Text>{note}</Text>
        </View>
      </Swipeout>
    )
  }
}

const styles = StyleSheet.create({ container: { height: 80 } })
