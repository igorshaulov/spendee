/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { StyleSheet, View, FlatList } from 'react-native'
import ListItem from './ListItem'

type Props = {
  transactions: Array<Transaction>,
  onTransactionDelete: Function,
}

export default class TrasactionsListView extends PureComponent<Props> {
  renderItem = (data: any) => {
    const { onTransactionDelete } = this.props
    const transaction: Transaction = data.item
    return (
      <ListItem
        key={transaction.id}
        transaction={transaction}
        index={data.index}
        onDelete={onTransactionDelete}
      />
    )
  }

  render = () => {
    const { transactions } = this.props
    return (
      <View style={styles.container}>
        <FlatList
          style={styles.list}
          data={transactions}
          renderItem={this.renderItem}
          keyExtractor={(transaction: Transaction) => transaction.id}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({ container: { flex: 1 }, list: { flex: 1 } })
