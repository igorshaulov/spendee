/**
 * @format
 * @flow
 */

export const PRIMARY_COLOR = '#009fe4'
export const GRAY_COLOR = '#ddd'
export const CELL_BG_EVEN = '#fefefe'
export const CELL_BG_ODD = '#f1f1f1'
export const GREEN_1 = '#2ECC40'
export const RED_1 = '#FF4136'
