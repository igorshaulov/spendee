/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import ScreenWrapper from 'spendee/src/components/UI/ScreenWrapper'
import User from 'spendee/src/containers/User'
import TransactionsList from 'spendee/src/containers/TransactionsList'
import withMenuNavigationOptions from 'spendee/src/navigation/withMenuNavigationOptions'

type Props = {}

export class Home extends PureComponent<Props> {
  render = () => (
    <ScreenWrapper>
      <User />
      <TransactionsList />
    </ScreenWrapper>
  )
}

export default withMenuNavigationOptions(Home, 'Home')
