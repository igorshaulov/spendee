/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { StyleSheet, FlatList } from 'react-native'
import ScreenWrapper from 'spendee/src/components/UI/ScreenWrapper'
import ListItem from './ListItem'

const ITEM_HOME = 'Home'
const ITEM_CATEGORIES = 'Categories'

type Props = {
  navigation: any,
}

export class Menu extends PureComponent<Props> {
  static navigationOptions = { title: 'Menu' }

  onItemPress = (item: string) => {
    switch (item) {
      case ITEM_HOME:
        this.closeMenuAndNavigate('Home')
        break
      case ITEM_CATEGORIES:
        this.closeMenuAndNavigate('Categories')
        break
      default:
        break
    }
  }

  closeMenuAndNavigate = (routeName: string) => {
    const { navigation } = this.props
    navigation.closeDrawer()
    navigation.navigate(routeName)
  }

  renderItem = (data: any) => {
    const title: string = data.item
    return <ListItem key={title} title={title} onPress={() => this.onItemPress(title)} />
  }

  render = () => {
    const data: Array<string> = [ITEM_HOME, ITEM_CATEGORIES]
    return (
      <ScreenWrapper>
        <FlatList
          style={styles.container}
          data={data}
          renderItem={this.renderItem}
          keyExtractor={(title: string) => title}
        />
      </ScreenWrapper>
    )
  }
}

const styles = StyleSheet.create({ container: { margin: 10 } })

export default Menu
