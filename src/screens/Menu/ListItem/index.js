/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'

type Props = {
  title: string,
  onPress: Function,
}

export default class ListItem extends PureComponent<Props> {
  render = () => {
    const { title, onPress } = this.props
    return (
      <TouchableOpacity style={styles.container} onPress={onPress}>
        <Text>{title}</Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({ container: { height: 40 } })
