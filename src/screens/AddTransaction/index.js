/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import ScreenWrapper from 'spendee/src/components/UI/ScreenWrapper'
import { View } from 'react-native'
import AddTransactionContainer from 'spendee/src/containers/AddTransaction'

type Props = {}

export class AddTransaction extends PureComponent<Props> {
  static navigationOptions = { title: 'Add Transaction' }

  render = () => (
    <ScreenWrapper>
      <AddTransactionContainer />
    </ScreenWrapper>
  )
}

export default AddTransaction
