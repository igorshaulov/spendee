/**
 * @format
 * @flow
 */

import React, { PureComponent } from 'react'
import ScreenWrapper from 'spendee/src/components/UI/ScreenWrapper'
import withMenuNavigationOptions from 'spendee/src/navigation/withMenuNavigationOptions'
import CategoriesList from 'spendee/src/containers/CategoriesList'

type Props = {}

export class Categories extends PureComponent<Props> {
  render = () => (
    <ScreenWrapper>
      <CategoriesList />
    </ScreenWrapper>
  )
}

export default withMenuNavigationOptions(Categories, 'Categories')
