/**
 * @format
 * @flow
 */

import createSagaMiddleware from 'redux-saga'
import { createStore, applyMiddleware } from 'redux'
import reducer from './reducers/rootReducer'
import rootSaga from './sagas/rootSaga'
import { APP_RESTORE_FROM_STORAGE } from './actions'

const sagaMiddleware = createSagaMiddleware()

export default function configureStore() {
  const store = createStore(reducer, applyMiddleware(sagaMiddleware))
  sagaMiddleware.run(rootSaga)
  store.dispatch({ type: APP_RESTORE_FROM_STORAGE })
  return store
}
