/**
 * @format
 * @flow
 */

import { all } from 'redux-saga/effects'
import user from './user'
import transactions from './transactions'
import categories from './categories'

export default function* rootSaga(): Generator<void, void, Function> {
  yield all([transactions(), user(), categories()])
}
