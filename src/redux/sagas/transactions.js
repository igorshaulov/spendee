/**
 * @format
 * @flow
 */

import { takeEvery, put } from 'redux-saga/effects'
import provider from 'spendee/src/dataProviders/Transaction'
import { TRANSACTIONS_ERROR,
  TRANSACTIONS_RESTORED,
  APP_RESTORE_FROM_STORAGE,
  TRANSACTIONS_ADD_ITEM,
  TRANSACTIONS_ITEM_ADDED,
  TRANSACTIONS_REMOVE_ITEM,
  TRANSACTIONS_ITEM_REMOVED } from '../actions'

function* addTransaction(action: Action): Generator<Object, void, Object> {
  const { transaction }: { transaction: Transaction } = action
  const result = yield provider.save(transaction, transaction.id)
  if (result) {
    yield put({ type: TRANSACTIONS_ITEM_ADDED, transaction })
  } else {
    yield put({ type: TRANSACTIONS_ERROR })
  }
}

function* removeTransaction(action: Action): Generator<Object, void, Object> {
  const { transaction }: { transaction: Transaction } = action
  const result = yield provider.remove(transaction.id)
  if (result) {
    yield put({ type: TRANSACTIONS_ITEM_REMOVED, transaction })
  } else {
    yield put({ type: TRANSACTIONS_ERROR })
  }
}

function* restoreTransactions() {
  const items = yield provider.loadAll()
  yield put({ type: TRANSACTIONS_RESTORED, items })
}

export default function* transactionsWatcher(): Generator<void, void, Object> {
  yield takeEvery(APP_RESTORE_FROM_STORAGE, restoreTransactions)
  yield takeEvery(TRANSACTIONS_ADD_ITEM, addTransaction)
  yield takeEvery(TRANSACTIONS_REMOVE_ITEM, removeTransaction)
}
