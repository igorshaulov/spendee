/**
 * @format
 * @flow
 */

import { takeEvery, put } from 'redux-saga/effects'
import provider from 'spendee/src/dataProviders/Category'
import uuid from 'uuid/v1'
import { APP_RESTORE_FROM_STORAGE,
  CATEGORIES_RESTORED,
  CATEGORIES_ITEM_ADDED,
  CATEGORIES_ERROR,
  CATEGORIES_ADD_ITEM,
  CATEGORIES_ITEM_REMOVED,
  CATEGORIES_REMOVE_ITEM } from '../actions'

function* addCategory(action: Action): Generator<Object, void, Object> {
  const { name } = action
  const category: Category = { id: uuid(), name }
  const result = yield provider.save(category, category.id)
  if (result) {
    yield put({ type: CATEGORIES_ITEM_ADDED, category })
  } else {
    yield put({ type: CATEGORIES_ERROR })
  }
}

function* removeCategory(action: Action): Generator<Object, void, Object> {
  const { category }: { category: Category } = action
  const result = yield provider.remove(category.id)
  if (result) {
    yield put({ type: CATEGORIES_ITEM_REMOVED, category })
  } else {
    yield put({ type: CATEGORIES_ERROR })
  }
}

function* restoreCategories() {
  const items: Array<Category> = yield provider.loadAll()
  if (items.length !== 0) {
    yield put({ type: CATEGORIES_RESTORED, items })
  } else {
    yield put({ type: CATEGORIES_ADD_ITEM, name: 'Default' })
  }
}

export default function* categoriesWatcher(): Generator<void, void, Object> {
  yield takeEvery(APP_RESTORE_FROM_STORAGE, restoreCategories)
  yield takeEvery(CATEGORIES_ADD_ITEM, addCategory)
  yield takeEvery(CATEGORIES_REMOVE_ITEM, removeCategory)
}
