/**
 * @format
 * @flow
 */

import { takeEvery, put, select } from 'redux-saga/effects'
import provider from 'spendee/src/dataProviders/User'
import { USER_BALANCE_CHANGED,
  APP_RESTORE_FROM_STORAGE,
  USER_RESTORED,
  TRANSACTIONS_ITEM_ADDED,
  TRANSACTIONS_ITEM_REMOVED } from '../actions'

function* saveBalance(balance: number) {
  yield put({ type: USER_BALANCE_CHANGED, balance })
  const state: Object = yield select()
  yield provider.save({ ...state.user, balance })
}

function* addBalance(action: Action) {
  const { transaction } = action
  const state: Object = yield select()
  const balance: number = state.user.balance + transaction.value
  yield saveBalance(balance)
}

function* substractBalance(action: Action) {
  const { transaction } = action
  const state: Object = yield select()
  const balance: number = state.user.balance - transaction.value
  yield saveBalance(balance)
}

function* restoreUser() {
  const userState = yield provider.load()
  if (userState) {
    yield put({ type: USER_RESTORED, userState })
  }
}

export default function* userWatcher(): Generator<void, void, Object> {
  yield takeEvery(APP_RESTORE_FROM_STORAGE, restoreUser)
  yield takeEvery(TRANSACTIONS_ITEM_ADDED, addBalance)
  yield takeEvery(TRANSACTIONS_ITEM_REMOVED, substractBalance)
}
