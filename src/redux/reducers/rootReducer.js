/**
 * @format
 * @flow
 */

import { combineReducers } from 'redux'
import transactions from './transactions'
import user from './user'
import categories from './categories'

export default combineReducers({ user, transactions, categories })
