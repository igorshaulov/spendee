/**
 * @format
 * @flow
 */

import { USER_BALANCE_CHANGED, USER_RESTORED } from '../actions'

const initialState: Object = { balance: 0 }

export default (state: Object = initialState, action: Action) => {
  switch (action.type) {
    case USER_RESTORED:
      return { ...action.userState }
    case USER_BALANCE_CHANGED:
      return { ...state, balance: action.balance }

    default:
      return state
  }
}
