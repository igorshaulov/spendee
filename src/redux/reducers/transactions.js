/**
 * @format
 * @flow
 */

import { TRANSACTIONS_ITEM_ADDED,
  TRANSACTIONS_RESTORED,
  TRANSACTIONS_ITEM_REMOVED } from '../actions'

const initialState: Object = { items: [] }

export default (state: Object = initialState, action: Action) => {
  switch (action.type) {
    case TRANSACTIONS_RESTORED:
      return { ...state, items: action.items }
    case TRANSACTIONS_ITEM_ADDED: {
      return { ...state,
        items: [action.transaction, ...state.items] }
    }
    case TRANSACTIONS_ITEM_REMOVED: {
      const { transaction: { id } } = action
      return { ...state,
        items: state.items.filter((transaction: Transaction) => transaction.id !== id) }
    }
    default:
      return state
  }
}
