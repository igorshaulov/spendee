/**
 * @format
 * @flow
 */

import { CATEGORIES_RESTORED, CATEGORIES_ITEM_ADDED, CATEGORIES_ITEM_REMOVED } from '../actions'

const initialState: Object = { items: [] }

export default (state: Object = initialState, action: Action) => {
  switch (action.type) {
    case CATEGORIES_RESTORED:
      return { ...state, items: action.items }
    case CATEGORIES_ITEM_ADDED:
      return { ...state,
        items: [action.category, ...state.items] }
    case CATEGORIES_ITEM_REMOVED: {
      const { category: { id } } = action
      return { ...state,
        items: state.items.filter((category: Category) => category.id !== id) }
    }

    default:
      return state
  }
}
